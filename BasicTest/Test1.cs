using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.IO;
using System.Linq;
using System.Reflection;

namespace BasicTest
{
    public class Tests
    {
        private IWebDriver _driver;
        public IWebDriver Driver => _driver;
        [SetUp]
        public void Setup()
        {
            _driver = new ChromeDriver();
            Driver.Manage().Window.Maximize();
        }

        [Test]
        public void Test1()
        {
            const string githubUrl = "https://github.com/";
            const string seleniumStr = "selenium";
            const string seleniumDocs = "https://selenium.dev/selenium/docs/api/dotnet/";
            Driver.Navigate().GoToUrl(githubUrl);
            Driver.FindElement(By.XPath("//summary[contains(text(), 'Why GitHub?')]")).Click();
            Driver.Navigate().GoToUrl(Driver.FindElement(By.XPath("//a[@href = '/features/actions']")).GetAttribute("href"));
            var searchFieldInput = Driver.FindElement(By.XPath("//input[contains(@class, 'header-search-input')]"));
            searchFieldInput.Click();
            searchFieldInput.SendKeys(seleniumStr);
            searchFieldInput.Submit();
            Driver.FindElement(By.XPath("//ul[@class = 'repo-list']/li[1]/div/h3/a")).SendKeys(Keys.Control + Keys.Return);
            Driver.SwitchTo().Window(Driver.WindowHandles.Last());
            Driver.Navigate().Refresh();
            Assert.AreEqual(Driver.FindElement(By.XPath("//a[text()='selenium']")).Text, seleniumStr);
            Driver.Close();
            Driver.SwitchTo().Window(Driver.WindowHandles.Last());
            Driver.Navigate().GoToUrl(seleniumDocs);
            var fileName = TestContext.CurrentContext.Test.MethodName.Replace("\"", "") + ".html";
            var fullFilePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), fileName);
            File.WriteAllText(fullFilePath, Driver.PageSource);
        }

        [TearDown]
        public void Dispose()
        {
            Driver.Quit();
        }
    }
}