using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System.Threading;

namespace Frames
{
    [TestFixture]
    public class Tests
    {
        private IWebDriver _driver;
        public IWebDriver Driver => _driver;
        [SetUp]
        public void Setup()
        {
            _driver = new ChromeDriver();
            Driver.Manage().Window.Maximize();
        }

        [Test]
        public void Test1()
        {
            const string iframeUrl = "https://html.com/tags/iframe/";
            const string videoName = "Massive volcanoes & Flamingo colony - Wild South America - BBC";
            const string contryCode = "BY";
            Driver.Navigate().GoToUrl(iframeUrl);
            var iframe = Driver.FindElement(By.XPath("//div[@class='render']/iframe"));
            new Actions(Driver)
                .MoveToElement(iframe)
                .Build()
                .Perform();
            Driver.SwitchTo().Frame(iframe);
            Thread.Sleep(2000);
            var videoLink = Driver.FindElement(By.XPath("//a[contains(@class,'ytp-title-link yt-uix-sessionlink')]"));
            Assert.AreEqual(videoLink.Text, videoName);
            Driver.Navigate().GoToUrl(videoLink.GetAttribute("href"));
            Thread.Sleep(300);
            Assert.AreEqual(Driver.FindElement(By.Id("country-code")).Text, contryCode);
        }
        [TearDown]
        public void Dispose()
        {
            Driver.Quit();
        }
    }
}